create database hello;
use hello;

create table login(username varchar(10) not null,password varchar(10) not null);
insert into login values("ravi",1234);
insert into login values("Chinni",789);
select * from login;

create table registration(username varchar(10) not null,password varchar(10) not null,email varchar(10));
insert into registration values("ravi",1234,ravi1999@gmail.com);
insert into registration values("Chinni",789,chinnig2000@gmail.com);

select * from registration;

create table books(bookid int primary key,booktitle varchar(10),bookgenre varchar(10),image varchar(20));
insert into books values(1," Rich Dad Poor Dad","Motivational","richdadpoordad.jpg");
insert into books values(2,"Clarissa","Romantic","clarissa.jpg");
insert into books values(3,"Alice in Wonderland"," cartoon","aliceinwonderland.jpg");
insert into books values(4,"Sybil","Horror","sybil.jpg");
insert into books values(5,"Little Women","ladyoriented","littlewomen.jpg");
insert into books values(6,"Frankenstein"," Horror"," frankenstein.jpg");

select * from books;