package com.assignment.books.controller;

import java.io.*;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import com.assignment.books.bean.Login;
import com.assignment.books.service.LoginService;

@WebServlet("/LoginController")
public class LoginController extends HttpServlet{
	
	public LoginController() {
		super();
	}

	private static final long serialVersionUID = 1L;
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		LoginService ls = new LoginService();
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
	
		PrintWriter pw = response.getWriter();
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		
		Login login = new Login();
		login.setUserName(username);
		login.setPassWord(password);
		
		LoginService ls = new LoginService();
		String res = ls.storeLogin(login);
						doGet(request, response);				
		pw.println(res);
		RequestDispatcher rd = request.getRequestDispatcher("home.jsp");
		rd.include(request, response);

		
	}


}
	

	
