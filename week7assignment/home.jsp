<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>BOOK STORE</title>
</head>
<body>
<div class="container">
		<div style="text-align:center;font-size:40px"> All the Books</div>
		<div class="row">
		
		<style>
		.ravi{
		float:left;
		background-color: lightgrey;
		height:450px;
  		width: 200px;
  		padding: 15px;
  		border:solid black;
  		margin: 10px;
  		color: light blue;
		}
		.Lucky{
		float:left;
		background-color: lightgrey;
		height:450px;
  		width: 200px;
  		padding: 15px;
  		border:solid black;
  		margin: 10px;
		}
		.Ameer{
		float:left;
		background-color: lightgrey;
		height:450px;
  		width: 200px;
  		padding: 15px;
  		border:solid black;
  		margin: 10px;
		}
		.Sudha{
		float:left;
		background-color: lightgrey;
		height:450px;
  		width: 200px;
  		padding: 15px;
  		border:solid black;
  		margin: 10px;
		}
		.Devi{
		float:left;
		background-color: lightgrey;
		height:450px;
  		width: 200px;
  		padding: 15px;
  		border:solid black;
  		margin: 10px;
		}
		.Shiva{
		float:left;
		background-color: lightgrey;
		height:450px;
  		width: 200px;
  		padding: 15px;
  		border:solid black;
  		margin: 10px;
		}
</style>
		
		<div class="ravi">
		<div class="col-md-3 my-3">
				<div class="book w-100">
					<img class="Book-img-top" src="image/positivethinking.jpg"
						alt="Book image cap">
					<div class="card-body">
						<h5 class="bookId" >BookId:1</h5>
						<h5 class="booktitle" >BookTitle:"positive thinking"</h5>
						<h6 class="bookgenre">Bookgenre:"motivatinal"</h6>
						<h6 class="bookauthor">Author:"Norman vincent"</h6>
						
						<div class="mt-3 d-flex justify-content-between">
							<a class="btn btn-dark" href="add-to-favourite?id=2"><button>ADD TO BAG</button></a> 
						</div>
					</div>
				</div>
			</div>
		</div>
		
		<div class="Lucky">
		
			<div class="col-md-3 my-3">
				<div class="book w-100">
					<img class="Book-img-top" src="image/wingsoffire.jpg"
						alt="Book image cap">
					<div class="card-body">
						<h5 class="bookId" >BookId:2</h5>
						<h5 class="booktitle" >BookTitle:"wings of fire"</h5>
						<h6 class="bookgenre">Bookgenre:"biography"</h6>
						<h6 class="bookauthor">Author:"Abdul kalam"</h6>
						
						<div class="mt-3 d-flex justify-content-between">
							<a class="btn btn-dark" href="add-to-favourite?id=1"><button>ADD TO BAG</button></a> 
						</div>
					</div>
				</div>	
			</div>	
		</div>	
			
			<div class="Ameer">
			<div class="col-md-3 my-3">
				<div class="book w-100">
					<img class="Book-img-top" src="image/dream.jpg"
						alt="Book image cap">
					<div class="card-body">
						<h5 class="bookId" >BookId:3</h5>
						<h5 class="booktitle" >BookTitle:"Your dreams are now mine"</h5>
						<h6 class="bookgenre">Bookgenre:"romantic"</h6>
						<h6 class="bookauthor">Author:"Ravinder Singh"</h6>
						
						<div class="mt-3 d-flex justify-content-between">
							<a class="btn btn-dark" href="add-to-favourite?id=2"><button>ADD TO  BAG</button></a> 
						</div>
					</div>
				</div>
			</div>
		</div>
			<div class="Sudha">
			<div class="col-md-3 my-3">
				<div class="book w-100">
					<img class="Book-img-top" src="image/fairytale.jpg"
						alt="Book image cap">
					<div class="card-body">
						<h5 class="bookId" >BookId:4</h5>
						<h5 class="booktitle" >BookTitle:"101 fairy tale"</h5>
						<h6 class="bookgenre">Bookgenre:"cartoon"</h6>
						<h6 class="bookauthor">Author:"Jacob Grimm"</h6>
						
						
						<div class="mt-3 d-flex justify-content-between">
							<a class="btn btn-dark" href="add-to-favourite?id=2"><button>ADD TO BAG</button></a> 
						</div>
					</div>
				</div>
			</div>
		</div>
			<div class="Devi">
			<div class="col-md-3 my-3">
				<div class="book w-100">
					<img class="Book-img-top" src="image/horror.jpg"
						alt="Book image cap">
					<div class="card-body">
						<h5 class="bookId" >BookId:5</h5>
						<h5 class="booktitle" >BookTitle:"The doll in garden"</h5>
						<h6 class="bookgenre">Bookgenre:"horror"</h6>
						<h6 class="bookauthor">Author:"Mary Downing"</h6>
						
						<div class="mt-3 d-flex justify-content-between">
							<a class="btn btn-dark" href="add-to-favourite?id=2"><button>ADD TO BAG</button></a> 
						</div>
					</div>
				</div>
			</div>
		</div>
			<div class="Shiva">
			<div class="col-md-3 my-3">
				<div class="book w-100">
					<img class="Book-img-top" src="image/crime.jpg"
						alt="Book image cap">
					<div class="card-body">
						<h5 class="bookId" >BookId:6</h5>
						<h5 class="booktitle" >BookTitle:"crime in forest"</h5>
						<h6 class="bookgenre">Bookgenre:"criminal"</h6>
						<h6 class="bookauthor">Author:"Satish Kumar"</h6>
						
						<div class="mt-3 d-flex justify-content-between">
							<a class="btn btn-dark" href="add-to-favourite?id=2"><button>ADD TO BAG</button></a> 
						</div>
					</div>
				</div>
			</div>
		</div>
			
		</div>
	</div>

</body>
</html>