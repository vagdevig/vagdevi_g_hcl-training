import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
@WebServlet("/gserv")
public class Servlet extends HttpServlet {
int count;
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	public Servlet() {
		System.out.println("hi");
	}
	@Override
	public void init() throws ServletException {
		// TODO Auto-generated method stub
		count=0;
		System.out.println("init");
	}
	
	@Override
	protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		String uid=req.getParameter("name");
		String pass=req.getParameter("psw");
		resp.setContentType("text/html");
		PrintWriter p=resp.getWriter();
		
			try {
				if( LoginServ.validate(uid, pass))
				{
					User u=new User();
					u.setUname(uid);
					RequestDispatcher r=req.getRequestDispatcher("Welcome.jsp");
					r.include(req, resp);
					
				}
				else
				{
					p.println("wrong password");
					RequestDispatcher r=req.getRequestDispatcher("signup.jsp");
					r.include(req, resp);
				}
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (ServletException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} 
		
	

	@Override
	protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(req, resp);
	}

	
	
}
