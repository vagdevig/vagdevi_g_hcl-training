package com.hcl.dao;
import com.hcl.model.*;
import java.sql.SQLException;
public interface IMovieDAO {
	public int add(Movie m)
		throws SQLException;
	public void delete(int id)
		 throws SQLException;
	public Movie getMovie(String category)
		 throws SQLException;
	public void update(Movie m)
		  throws SQLException;
}
