package com;
public class SuperDepartment
{
	public String deparmentName() 
	{
		return "Super Department";
	}
	public String getTodaysWork(int c) 
	{
		if(c==1)
			return "Complete your documents Submission";
		else if(c==2)
			return "Fill todays worksheet and mark your attendance";
		else if(c==3)
			return "Complete coding of module 1";
		else
			return "No work as of now";
	}
	public String getWorkDeadline(int c) 
	{
		if(c>3)
			return "Nil";
		else 
			return "Complete by EOD";
	}
	public String isTodayAHoliday(String s)
	{
		if(s.equals("y"))
			return "Today is not a holiday";
		else
			return "Today is a holiday";
	}
}




