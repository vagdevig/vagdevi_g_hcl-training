create database travel_vagdevi;
use travel_vagdevi;
create table PASSENGER(
Passenger_name varchar(20), 
  Category               varchar(20),
   Gender                 varchar(20),
   Boarding_City      varchar(20),
   Destination_City   varchar(20),
  Distance                int,
  Bus_Type             varchar(20));

create table PRICE(
             Bus_Type   varchar(20),
             Distance    int,
              Price      int
          );

insert into passenger values('Sejal','AC','F','Bengaluru','Chennai',350,'Sleeper');
insert into passenger values('Anmol','Non-AC','M','Mumbai','Hyderabad',700,'Sitting');
insert into passenger values('Pallavi','AC','F','panaji','Bengaluru',600,'Sleeper');
insert into passenger values('Khusboo','AC','F','Chennai','Mumbai',1500,'Sleeper');
insert into passenger values('Udit','Non-AC','M','Trivandrum','panaji',1000,'Sleeper');
insert into passenger values('Ankur','AC','M','Nagpur','Hyderabad',500,'Sitting');
insert into passenger values('Hemant','Non-AC','M','panaji','Mumbai',700,'Sleeper');
insert into passenger values('Manish','Non-AC','M','Hyderabad','Bengaluru',500,'Sitting');
insert into passenger values('Piyush','AC','M','Pune','Nagpur',700,'Sitting');

select * from passenger;
insert into price values('Sleeper',350,770);
insert into price values('Sleeper',500,1100);
insert into price values('Sleeper',600,1320);
insert into price values('Sleeper',700,1540);
insert into price values('Sleeper',1000,2200);
insert into price values('Sleeper',1200,2640);
insert into price values('Sleeper',350,434);
insert into price values('Sitting',500,620);
insert into price values('Sitting',500,620);
insert into price values('Sitting',600,744);
insert into price values('Sitting',700,868);
insert into price values('Sitting',1000,1240);
insert into price values('Sitting',1200,1488);
insert into price values('Sitting',1500,1860);
select * from price;

/* 1 */
select count(*) gender
 from passenger 
 where distance<=600 group by gender like '%F'
 order by gender like '%M';
 
/*2*/
select min(Price) 
from Price where Bus_Type='Sleeper';

/*3*/
SELECT * FROM passenger WHERE Passenger_name LIKE 'S%';

/*4*/
select  Passenger.Passenger_name,Passenger.Boarding_City,Passenger.Destination_City,Passenger.Bus_Type,Price.Price 
from passenger ,price 
where Passenger.Distance=Price.Distance and Passenger.Bus_Type=Price.Bus_Type group by Passenger.Passenger_name;

/*5*/
select PAssenger.Passenger_name,Price.Price 
from passenger ,price  
where Passenger.Bus_Type='Sitting' and Passenger.Distance=1000;

/*6*/
select Price from price,passenger 
where  passenger.Distance=price.Distance and passenger.Passenger_name="Pallavi";

/*7*/
select  distinct distance
from Passenger
 order by distance desc;
 
 /*8*/
 select  Passenger_name,(Distance/c.h)*100 as percentage 
 from passenger cross join (select sum(Distance) as h from passenger)c;
 
 /*9*/
create view pass as select * from passenger
where Category='Ac';
select * from pass;

/*10*/
delimiter //
create procedure storedpassenger() begin
select count(*) from passenger where Bus_type='Sleeper';
	End //
call storedpassenger();

/*11*/
SELECT * FROM passenger ORDER BY passenger_name  LIMIT 5;