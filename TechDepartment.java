package com;
public class TechDepartment extends SuperDepartment
{
	public String deparmentName() 
	{
		return "Tech Department";
	}
	public String getTodaysWork(int c) 
	{
		return new SuperDepartment().getTodaysWork(c);
	}
	public String getWorkDeadline(int c) 
	{
		return new SuperDepartment().getWorkDeadline(c);
	}
	public String getTechStackInformation()
	{
		return "core Java";
	}
	public String isTodayAHoliday(String s)
	{
		return new SuperDepartment().isTodayAHoliday(s);
	}
}




