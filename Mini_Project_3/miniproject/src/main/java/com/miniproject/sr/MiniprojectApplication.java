package com.miniproject.sr;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.jdbc.core.JdbcTemplate;

import com.miniproject.sr.pojo.AdminLogin;
import com.miniproject.sr.pojo.Items;
import com.miniproject.sr.pojo.LoginUser;
import com.miniproject.sr.repository.AdminLogRepository;
import com.miniproject.sr.repository.ItemRepository;
import com.miniproject.sr.repository.UserRepository;




@SpringBootApplication
public class MiniprojectApplication implements CommandLineRunner {

	public static void main(String[] args) {
		
		SpringApplication.run(MiniprojectApplication.class, args);
	}
	@Autowired
	private UserRepository userRepository;
	
	@Autowired
	private ItemRepository bookRepository;
	
	@Autowired
	private AdminLogRepository adminLogRepository;
	
	@Autowired
	private JdbcTemplate jdbcTemplate;
	public void run() {
		
	}
	@Override
	public void run(String... args) throws Exception {
		String sql="set FOREIGN_KEY_CHECKS=0";
		int result=jdbcTemplate.update(sql);
	}
	
	@Bean
	public void inserData(){
		LoginUser login1=new LoginUser("Aman","SK","aman@gmail.com","1001001001","123","123");
		userRepository.save(login1);
		
		LoginUser login2=new LoginUser("Cherry","G","cherryg@gmail.com","98880100","cherry12","cherry12");
		userRepository.save(login2);
		
		LoginUser login3=new LoginUser("Gagana","M","gagana@gmail.com","1234567890","g789","g789");
		userRepository.save(login3);
		
		LoginUser login4=new LoginUser("Varsha","K","varsha@test.com","768999998","varsha","varsha");
		userRepository.save(login4);
		
		AdminLogin admin1=new AdminLogin("tinku", "KK", "tinku@gmail.com", "999999999", "tinkukk", "tinkukk");
		adminLogRepository.save(admin1);

		Items book=new Items("Veg Roll","69","1","IMG001");
		bookRepository.save(book);
		
		Items book1=new Items("Burgur","129","1","IMG002");
		bookRepository.save(book1);
		
		Items book2=new Items("Samosa","20","4","IMG003");
		bookRepository.save(book2);
		
		Items book3=new Items("South India Meal ","189","3","IMG004");
		bookRepository.save(book3);
		
		Items book4=new Items("Parata","45","2","IMG005");
		bookRepository.save(book4);
		
		
	}


}
